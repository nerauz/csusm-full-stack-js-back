module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions:  {
    project: './tsconfig.json',
    sourceType: "module",
    ecmaVersion: 6
  },
  extends: [
    "airbnb-base/legacy"
  ],
  ignorePatterns: ['.eslintrc.js'],
  rules: {
    "no-shadow": "off",
    "no-underscore-dangle": "off",
    "no-unused-vars": "off",
    "@typescript-eslint/no-shadow": ["error"]
  },
  plugins: [
    '@typescript-eslint'
  ]
};