import mongoose from 'mongoose';
import { ISummoner } from './interfaces/ISummoner';

const summonerSchema: mongoose.Schema = new mongoose.Schema({
  id: { type: String, index: true },
  accountId: { type: String },
  puuid: { type: String },
  name: { type: String, index: true },
  profileIconId: { type: Number },
  revisionDate: { type: Number },
  summonerLevel: { type: Number },
  champMastery: { type: Number, index: true },
  champId: { type: Number }
}, { timestamps: true });

summonerSchema.methods.toJson = function (): Object {
  return {
    id: this.id,
    name: this.name,
    summonerLevel: this.summonerLevel,
    profileIconId: this.profileIconId
  };
};

summonerSchema.methods.toMigrate = function (): Object {
  return {
    id: this.id,
    name: this.name,
    summonerLevel: this.summonerLevel,
    profileIconId: this.profileIconId,
    accountId: this.accountId,
    puuid: this.puuid,
    revisionDate: this.revisionDate,
    champMastery: this.champMastery,
    champId: this.champId,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt
  };
};

const Summoner: mongoose.Model<ISummoner> = mongoose.model('Summoner', summonerSchema);

export { Summoner };
