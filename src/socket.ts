import io from 'socket.io';
import http from 'http';

class SocketServer extends io.Server {
  private baseRoomName: string = 'room';

  constructor(server: http.Server) {
    super(server, { cors: { origin: 'http://localhost:8000', methods: ['GET', 'POST'] } });
  }

  start(): void {
    this.on('connection', (socket) => {
      console.log('user connected');

      socket.on('disconnect', () => {
        console.log('user disconnected');
      });

      socket.on('join', (data: { room: string, name: string; }) => {
        socket.join(`${this.baseRoomName}-${data.room}`);

        socket.to(`${this.baseRoomName}-${data.room}`).emit('join', { msg: 'joined the room', name: data.name });
      });

      socket.on('send-to-room', (data: {room: string, name: string, message: string}) => {
        socket.to(`${this.baseRoomName}-${data.room}`).emit('message', { msg: data.message, name: data.name });
      });
    });
  }
}

export { SocketServer };
