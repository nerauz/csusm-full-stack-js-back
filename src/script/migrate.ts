import { Database } from '../database';
import mongoose, { Model } from 'mongoose';
import { User } from '../models/user';
import { IUser } from '../models/interfaces/IUser';
import { ISummoner } from '../models/interfaces/ISummoner';
import { Summoner } from '../models/summoner';
import { IGeneric } from '../models/interfaces/generic';

async function addToDb<T extends IGeneric>(elements: T[], NewModel: Model<T>): Promise<void> {
  // eslint-disable-next-line no-restricted-syntax
  for (const element of elements) {
    const json: Object = element.toMigrate();
    const result: T = new NewModel(json);

    // eslint-disable-next-line no-await-in-loop
    await result.save();

    console.log(`Object: ${result._id} successfully migrated`);
  }
}

function migrate(from: string, to: string): void {
  Database.connect(async () => {
    console.log(`Connected to ${from} to get data`);

    const users: IUser[] = await User.find({});
    const otp: ISummoner[] = await Summoner.find({});

    mongoose.disconnect().then(() => {
      console.log(`Connection to ${from} closed`);

      Database.connect(async () => {
        console.log(`Connected to ${to} to fill data`);

        await addToDb<IUser>(users, User);
        await addToDb<ISummoner>(otp, Summoner);

        mongoose.disconnect().then(() => {
          console.log(`Connection to ${to} closed`);
        });
      }, to);
    });
  }, from);
}

function main() {
  const from: string = process.argv[2];
  const to: string = (process.argv[3]);

  migrate(from, to);
}

main();
