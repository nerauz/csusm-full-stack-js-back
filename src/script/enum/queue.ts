enum EQueue {
    // eslint-disable-next-line camelcase
    RANKED_SOLO_5x5 = 'RANKED_SOLO_5x5',
    RANKED_FLEX_SR = 'RANKED_FLEX_SR',
    RANKED_FLEX_TT = 'RANKED_FLEX_TT'
}

export { EQueue };
