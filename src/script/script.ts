import { AxiosResponse } from 'axios';
import { IChallenger } from './interface/challenger';
import { ISummoner } from './interface/summoner';
import { IChampion } from './interface/champion';
import { IEntry } from './interface/entry';
import { ERegion } from './enum/region';
import { EChampion } from './enum/champion';
import { EQueue } from './enum/queue';
import { Riot } from './riot';
import { Database } from '../database';
import { Summoner } from '../models/summoner';
import mongoose from 'mongoose';

async function getOtp(
  riot: Riot,
  champId: number,
  challengers: IEntry[]
): Promise<ISummoner[]> {
  const otp: ISummoner[] = [];

  // eslint-disable-next-line no-restricted-syntax
  for (const challenger of challengers) {
    try {
      // eslint-disable-next-line no-await-in-loop
      const summoner: AxiosResponse<ISummoner> = await riot
        .getSummonerProfile(challenger.summonerId);
      // eslint-disable-next-line no-await-in-loop
      const champ: AxiosResponse<IChampion> = await riot
        .getMastery(challenger.summonerId, champId);

      if (champ.data.lastPlayTime > riot.lastPatchDate && champ.data.championLevel >= 5) {
        summoner.data.champMastery = champ.data.championPoints;
        summoner.data.champId = champId;
        otp.push(summoner.data);
      }
    } catch (e) {
      if (e.response.status === 404) {
        console.error(`Champion: ${champId} for ${challenger.summonerName} not found.`);
      }
    }
  }

  otp.sort((a, b) => b.champMastery - a.champMastery);

  return otp.slice(0, 10);
}

async function addToDatabase(summoner: ISummoner) {
  try {
    const mongoSummoner = new Summoner(summoner);

    await mongoSummoner.save();

    console.log(`Added summoner name: ${summoner.name} for champ id: ${summoner.champId}`);
  } catch (e) {
    console.error(`summoner name: ${summoner.name}, champ id: ${summoner.champId},  error: ${e}`);
  }
}

function fillDatabase(): void {
  Database.connect(async () => {
    const riot: Riot = new Riot(ERegion.EUW1, EQueue.RANKED_SOLO_5x5);
    const challengerRes: AxiosResponse<IChallenger> = await riot.getChallengerFromRegion();
    const entries: IEntry[] = challengerRes.data.entries
      .sort((a, b) => b.leaguePoints - a.leaguePoints);
    const enumValues: (string | EChampion)[] = Object.values(EChampion);

    // eslint-disable-next-line no-restricted-syntax
    for (const value of enumValues) {
      const champId: number = Number(value);

      if (!Number.isNaN(champId)) {
        // eslint-disable-next-line no-await-in-loop
        const summoners: ISummoner[] = await getOtp(riot, champId, entries);

        // eslint-disable-next-line no-restricted-syntax
        for (const summoner of summoners) {
          // eslint-disable-next-line no-await-in-loop
          await addToDatabase(summoner).then();
        }
      }
    }

    mongoose.disconnect().then(() => {
      console.log('Connection to db closed');
    });
  });
}

function main(): void {
  const env: string = process.env.NODE_ENV;

  if (!env) {
    console.error('Please set your NODE_ENV variable to one of this: dev - test - prod');
    return;
  }

  fillDatabase();
}

main();
