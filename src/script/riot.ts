import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { IPatchResponse } from './interface/patchResponse';
import { IPatch } from './interface/patch';
import { ISummoner } from './interface/summoner';
import { IChampion } from './interface/champion';
import { IChallenger } from './interface/challenger';
import { ERegion } from './enum/region';
import { EQueue } from './enum/queue';
import { Environment } from '../shared/environment';

class Riot {
    private patchUrl: string = 'https://raw.githubusercontent.com/CommunityDragon/Data/master/patches.json';

    public lastPatchDate: number;

    private instance: AxiosInstance;

    private readonly region: string;

    private readonly queue: string;

    constructor(region: ERegion, queue: EQueue) {
      this.region = region.toString().toLocaleLowerCase();
      this.queue = queue.toString();

      this.getLastPatchDate();
      this.setAxiosHeader();
    }

    setAxiosHeader():void {
      this.instance = axios.create({
        headers: {
          common: { // can be common or any other method
            'X-Riot-Token': Environment.apiKey
          }
        }
      });
    }

    getLastPatchDate(): void {
      axios.get(this.patchUrl).then((res: AxiosResponse<IPatchResponse>) => {
        const patches: [IPatch] = res.data.patches;

        this.lastPatchDate = patches[patches.length - 1].start;
      });
    }

    getSummonerProfile(summonerId: string): Promise<AxiosResponse<ISummoner>> {
      return this.instance.get(`https://${this.region}.api.riotgames.com/lol/summoner/v4/summoners/${summonerId}`);
    }

    getMastery(summonerId: string, champId: number)
        : Promise<AxiosResponse<IChampion>> {
      return this.instance.get(`https://${this.region}.api.riotgames.com/lol/champion-mastery/v4/champion-masteries/by-summoner/${summonerId}/by-champion/${champId}`);
    }

    getChallengerFromRegion(): Promise<AxiosResponse<IChallenger>> {
      return this.instance.get(`https://${this.region}.api.riotgames.com/lol/league/v4/challengerleagues/by-queue/${this.queue}`);
    }
}

export { Riot };
