interface IPatch {
    name: string,
    start: number,
    season: number
}

export { IPatch };
