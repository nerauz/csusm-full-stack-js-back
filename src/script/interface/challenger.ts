import { IEntry } from './entry';

interface IChallenger {
    tier: string,
    leagueId: string,
    queue: string,
    name: string,
    entries: [IEntry]
}

export { IChallenger };
