import { IPatch } from './patch';

interface IPatchResponse {
    patches: [IPatch],
    shifts: Map<String, number>
}

export { IPatchResponse };
