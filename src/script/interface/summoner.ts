interface ISummoner {
    id: string,
    accountId: string,
    puuid: string,
    name: string,
    profileIconId: number,
    revisionDate: number,
    summonerLevel: number,
    champMastery?: number,
    champId?: number
}

export { ISummoner };
