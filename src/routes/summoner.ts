import express from 'express';

import { ResponseWrapper } from '../response';
import { ResourceNotFoundError } from '../error/resourceNotFound';
import { ARoute } from './interface/ARoute';
import { Summoner } from '../models/summoner';
import { ISummoner } from '../models/interfaces/ISummoner';

class SummonerRoute extends ARoute {
  constructor(app: express.Application) {
    super(app);
  }

  getOtp(): void {
    this.app.get('/otp', async (req: any, res: any) => {
      try {
        const summoners: ISummoner[] = await Summoner.find({ champId: req.query.champId }).sort({ champMastery: 'desc' }).exec();
        const summonersToJson: Object[] = summoners.map((summoner: ISummoner) => summoner.toJson());

        if (summoners === null || summoners.length <= 0) throw new ResourceNotFoundError('The given champ id does not match');
        else ResponseWrapper.ok(res, summonersToJson);
      } catch (e) {
        if (e.data && e.data.code === 404) ResponseWrapper.notFound(res, e.data.error);
        else ResponseWrapper.badRequest(res);
      }
    });
  }

  setRoutes(): void {
    this.getOtp();
  }
}

export { SummonerRoute };
